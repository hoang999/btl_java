/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.extend;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import views.content.XemDiemLop;

/**
 *
 * @author Hoang
 */
public class LuuFile {

    File curentFile;
    JFileChooser chooser;

    public LuuFile() {
    }

    public void ghiFile(JTable table) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            
            chooser.setAcceptAllFileFilterUsed(false);
            chooser.setFileFilter(new FileNameExtensionFilter("XLS file (.xls)","xls"));
            int res = chooser.showSaveDialog(null);
            if (res != JFileChooser.APPROVE_OPTION) {
                return;
            }
            curentFile = chooser.getSelectedFile();
            if(!curentFile.getName().endsWith("xls")){
                curentFile = new File(curentFile.getAbsoluteFile() +".xls");
            }
            write(curentFile, table);
        } catch (IOException | ClassNotFoundException
                | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(XemDiemLop.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void write(File file, JTable table) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("NewSheet");
        Row row;
        Cell cell;
        row = sheet.createRow(0);

        TableModel model = table.getModel();
        for (int i = 0; i < model.getColumnCount(); i++) {
            cell = row.createCell(i, CellType.STRING);
            cell.setCellValue(model.getColumnName(i));
        }
        for (int i = 0; i < model.getRowCount(); i++) {
            row = sheet.createRow(i + 1);
            for (int j = 0; j < model.getColumnCount(); j++) {
                cell = row.createCell(j);
                cell.setCellValue(model.getValueAt(i, j).toString());
            }
        }
        workbook.write(new FileOutputStream(file));
        JOptionPane.showMessageDialog(null,"Lưu thành công " + file.getAbsolutePath());
    }
}
