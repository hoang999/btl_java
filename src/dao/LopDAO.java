/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Lop;
import entities.MonHoc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hoang
 */
public class LopDAO implements DAO<Lop, Integer> {

    private static Connection conn = DBConection.getConnection();
    private static LopDAO instance = new LopDAO();

    public static LopDAO instance() {
        return instance;
    }

    @Override
    public List<Lop> findAll() {
        List<Lop> list = new ArrayList<Lop>();
        String sql = "select * from LOP";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            ResultSet rs = pr.executeQuery();
            while (rs.next()) {
                Lop lop = new Lop();
                lop.setMalop(rs.getInt("malop"));
                lop.setTenlop(rs.getString("tenlop"));
                lop.setKhoa(rs.getInt("khoa"));
                lop.setMagv("gvcn");
                list.add(lop);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Lop findByID(Integer id) {
        try {
            String sql = "select * from LOP where malop=?";
            PreparedStatement pr;
            pr = conn.prepareStatement(sql);
            pr.setInt(1, id);
            ResultSet rs = pr.executeQuery();
            if (rs.next()) {
                Lop lop = new Lop();
                lop.setMalop(rs.getInt("malop"));
                lop.setTenlop(rs.getString("tenlop"));
                lop.setKhoa(rs.getInt("khoa"));
                lop.setMagv(rs.getString("gvcn"));
                return lop;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean insert(Lop model) {
        String sql = "insert into LOP (malop,tenlop,khoa) values(?,?,?)";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setInt(1, model.getMalop());
            pr.setString(2, model.getTenlop());
            pr.setInt(3, model.getKhoa());

            int sl = pr.executeUpdate();
            if (sl > 0) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;

        }

        return false;
    }

    @Override
    public boolean delete(Integer modelID) {
        try {
            String sql = "delete from LOP where malop=?";
            PreparedStatement pr;
            pr = conn.prepareStatement(sql);
            int sl = pr.executeUpdate();
            if (sl > 0) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    @Override
    public boolean update(Lop model) {
        try {
            String sql = "UPDATE LOP "
                    + "SET malop=?,tenlop=?,khoa=?";
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setInt(1, model.getMalop());
            pr.setString(2, model.getTenlop());
            pr.setInt(3, model.getKhoa());
            int sl = pr.executeUpdate();
            if (sl > 0) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    

}
