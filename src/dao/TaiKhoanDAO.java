/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import entities.TaiKhoan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Hoang
 */
public class TaiKhoanDAO implements DAO<TaiKhoan, String>{
    
    public TaiKhoan login(String id, String password){
        try {
            TaiKhoan tk = null;
            Connection conn = DBConection.getConnection();
            String sql = "select * from TAIKHOAN where taikhoan=?  and matkhau=?";
            PreparedStatement ptmt = conn.prepareStatement(sql);
            ptmt.setString(1, id);
            ptmt.setString(2, password);
            ResultSet rs = ptmt.executeQuery();
            if(rs.next()){
                tk = new TaiKhoan();
                tk.setTaikhoan(rs.getString("taikhoan"));
                tk.setVaitro(rs.getString("vaitro"));
                tk.setLogon(rs.getBoolean("logon"));
                tk.setMatkhau(rs.getString("matkhau"));
            }
            return tk;
        } catch (SQLException ex) {
            Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean update(TaiKhoan tk) {
        try {
            String sql = "update taikhoan set taikhoan.matkhau=?, taikhoan.logon='true' where taikhoan=?";
            PreparedStatement ptmt = DBConection.getConnection().prepareStatement(sql);
            ptmt.setString(2, tk.getTaikhoan());
            ptmt.setString(1, tk.getMatkhau());
            ptmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(TaiKhoanDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public List<TaiKhoan> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TaiKhoan findByID(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(TaiKhoan model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(String modelID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
