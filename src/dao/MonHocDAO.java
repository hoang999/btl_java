/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Lop;
import entities.MonHoc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hoang
 */
public class MonHocDAO implements DAO<MonHoc, Integer>{
    private static Connection conn = DBConection.getConnection();
    private static MonHocDAO instance = new MonHocDAO();
    public static MonHocDAO instance(){return instance;};
    
    @Override
    public MonHoc findByID(Integer id){
        MonHoc mon = null;
        String sql = "select * from monhoc where mamon=?";
        try {
            PreparedStatement ptmt = conn.prepareStatement(sql);
            ptmt.setInt(1, id);
            ResultSet rs = ptmt.executeQuery();
            if(rs.next()){
                mon = new MonHoc();
                mon.setMamon(rs.getInt("mamon"));
                mon.setTenmon(rs.getString("tenmon"));
                return mon;
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mon;
    }

    /**
     *
     * @return 
     */
    @Override
    public List<MonHoc> findAll(){
        List<MonHoc> list = new ArrayList<>();
        MonHoc mon = null;
        String sql = "select * from monhoc";
        try {
            PreparedStatement ptmt = conn.prepareStatement(sql);
            ResultSet rs = ptmt.executeQuery();
            while(rs.next()){
                mon = new MonHoc();
                mon.setMamon(rs.getInt("mamon"));
                mon.setTenmon(rs.getString("tenmon"));
                list.add(mon);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public List<MonHoc> timMonChuaDay(Integer malop) {
        ArrayList<MonHoc> list = new ArrayList<MonHoc>();
        try {
            String sql = "select * from MONHOC where mamon not in\n"
                    + " (select GIANGDAY.mamon from lop inner join GIANGDAY on LOP.malop = GIANGDAY.malop\n"
                    + " inner join MONHOC on MONHOC.mamon = GIANGDAY.mamon where LOP.malop=?)";
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setInt(1, malop);
            ResultSet rs = pr.executeQuery();
            while(rs.next()){
                MonHoc mon = new MonHoc();
                mon.setMamon(rs.getInt("mamon"));
                mon.setTenmon(rs.getString("tenmon"));
                list.add(mon);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LopDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    @Override
    public boolean insert(MonHoc model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Integer modelID) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(MonHoc model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
