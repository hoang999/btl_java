/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import entities.GiaoVien;
import entities.Lop;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Hoang
 */
public class GiaoVienDAO implements DAO<GiaoVien, String>{
    private static Connection conn = DBConection.getConnection();
    private static GiaoVienDAO instance = new GiaoVienDAO();
    public static GiaoVienDAO newInstance(){return instance;};

    @Override
    public List<GiaoVien> findAll() {
        List<GiaoVien> list = new ArrayList<>();
        String sql = "SELECT * FROM GIAOVIEN";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            ResultSet rs = pr.executeQuery();
            while(rs.next()){
                GiaoVien gv = new GiaoVien();
                gv.setMagv(rs.getString("magv"));
                gv.setTengv(rs.getString("tengv"));
                gv.setGioitinh(rs.getString("gioitinh"));
                gv.setChunhiemlop(rs.getInt("chunhiemlop"));
                gv.setNgaysinh(rs.getDate("ngaysinh"));
                gv.setMamon(rs.getInt("monhoc"));
                list.add(gv);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GiaoVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
        }

    @Override
    public GiaoVien findByID(String id) {
        String sql = "SELECT * FROM GIAOVIEN where magv=?";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setString(1, id);
            ResultSet rs = pr.executeQuery();
            if(rs.next()){
                GiaoVien gv = new GiaoVien();
                gv.setMagv(rs.getString("magv"));
                gv.setTengv(rs.getString("tengv"));
                gv.setGioitinh(rs.getString("gioitinh"));
                gv.setChunhiemlop(rs.getInt("chunhiemlop"));
                gv.setNgaysinh(rs.getDate("ngaysinh"));
                gv.setMamon(rs.getInt("monhoc"));
                return gv;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean insert(GiaoVien model) {
         String sql = "INSERT INTO GIAOVIEN(magv,tengv,chunhiemlop,gioitinh,ngaysinh,monhoc) "
                + " VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setString(1, model.getMagv());
            pr.setString(2, model.getTengv());
            pr.setInt(3, model.getChunhiemlop());
            pr.setString(3, model.getGioitinh());
            pr.setDate(5, model.getNgaysinh());
            pr.setInt(6, model.getMamon());
            int sl = pr.executeUpdate();
            if(sl>0) return true; 
        } catch (SQLException ex) {
            Logger.getLogger(GiaoVienDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    @Override
    public boolean delete(String modelID) {
     String sql = "DELETE FROM GIAOVIEN WHERE magv=?";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setString(1, modelID);
            int sl = pr.executeUpdate();
            if(sl>0) return true;
        } catch (SQLException ex){
            ex.printStackTrace();
            return false;
        }
        return false;   
    }

    @Override
    public boolean update(GiaoVien model) {
        String sql = "UPDATE GIAOVIEN "
                + "SET monhoc=?,tengv=?,chunhiemlop=?,gioitinh=?, ngaysinh=? where magv=?";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setInt(1, model.getMamon());
            pr.setString(2, model.getTengv());
            pr.setInt(3, model.getChunhiemlop());
            pr.setString(4, model.getGioitinh());
            pr.setDate(5, model.getNgaysinh());
            pr.setString(6, model.getMagv());
            int sl = pr.executeUpdate();
            if(sl>0) return true;
        } catch (SQLException ex) {
            Logger.getLogger(GiaoVienDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }  

    public List<GiaoVien> timGiaoVienDayMon(int mamon) {
        List<GiaoVien> list = new ArrayList<>();
        String sql = "select * from giaovien where monhoc=?";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setInt(1, mamon);
            ResultSet rs = pr.executeQuery();
            while(rs.next()){
                GiaoVien gv = new GiaoVien();
                gv.setMagv(rs.getString("magv"));
                gv.setTengv(rs.getString("tengv"));
                gv.setGioitinh(rs.getString("gioitinh"));
                gv.setChunhiemlop(rs.getInt("chunhiemlop"));
                gv.setNgaysinh(rs.getDate("ngaysinh"));
                gv.setMamon(rs.getInt("monhoc"));
                list.add(gv);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GiaoVienDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
