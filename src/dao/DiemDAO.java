/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Diem;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hoang
 */
public class DiemDAO implements DAO<Diem, Integer>{

    private static Connection conn = DBConection.getConnection();
    private static DiemDAO instance = new DiemDAO();

    public static DiemDAO instance() {
        return instance;
    }

    @Override
    public List<Diem> findAll() {
        List<Diem> list = new ArrayList<Diem>();
        String sql = "select * from DIEM";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            ResultSet rs = pr.executeQuery();
            while(rs.next()){
                Diem diem = new Diem();
                diem.setDiem(rs.getDouble("diem"));
                diem.setLoai(rs.getString("Loai"));
                diem.setMahs(rs.getString("Mahs"));
                diem.setMamon(rs.getInt("mamon"));
                list.add(diem);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DiemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<Diem> findByMaHS(String id) {
        List<Diem> list = new ArrayList<>();
        try {
            String sql = "select * from DIEM where mahs=?";
            PreparedStatement pr;
            pr = conn.prepareStatement(sql);
            pr.setString(1, id);
            ResultSet rs = pr.executeQuery();
            while(rs.next()){
                Diem diem = new Diem();
                diem.setDiem(rs.getDouble("Diem"));
                diem.setLoai(rs.getString("Loai"));
                diem.setMahs(rs.getString("Mahs"));
                diem.setMamon(rs.getInt("Mamon"));
                list.add(diem);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DiemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public boolean insert(Diem model) {
        String sql = "INSERT INTO DIEM(mahs,mamon,diem,loai) values(?,?,?,?)";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setString(1, model.getMahs());
            pr.setInt(2, model.getMamon());
            pr.setDouble(3, model.getDiem());
            pr.setString(4, model.getLoai());
            int sl = pr.executeUpdate();
            if(sl>0) return true; 
            
        } catch (SQLException ex) {
            Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    @Override
    public boolean delete(Integer modelID) {
        String sql = "DELETE FROM DIEM WHERE madiem=?";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setInt(1, modelID);
            int sl = pr.executeUpdate();
            if(sl>0) return true;
        } catch (SQLException ex){
            ex.printStackTrace();
            return false;
        }
        return false;
    }

    @Override
    public boolean update(Diem model) {
        String sql = "UPDATE DIEM "
                + "SET mahs=?,mamon=?,diem=?, loai=?, where madiem=?";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setString(1, model.getMahs());
            pr.setInt(2, model.getMamon());
            pr.setDouble(3, model.getDiem());
            pr.setString(4, model.getLoai());
            pr.setInt(5, model.getMadiem());
            int sl = pr.executeUpdate();
            if(sl>0) return true;
        } catch (SQLException ex) {
            Logger.getLogger(DiemDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    @Override
    public Diem findByID(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public double tinhDiemTB(String mahs, int mamonhoc){
        try {
            String sql = "select dbo.tinhDiemTb(?,?)";
            CallableStatement st = conn.prepareCall(sql);
            st.setString(1, mahs);
            st.setInt(2, mamonhoc);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getDouble(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DiemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    public double tinhDiemTBHS(String mahs){
        try {
            String sql = "select dbo.tinhDiemTbHs(?)";
            CallableStatement st = conn.prepareCall(sql);
            st.setString(1, mahs);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return rs.getDouble(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DiemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }    
}
