/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hoang
 */
public class DBConection{
    public static Connection getConnection(){
        Properties pro = new Properties();
        try {
            pro.load(new FileInputStream("database.properties"));
        } catch (IOException  ex) {
            Logger.getLogger(DBConection.class.getName()).log(Level.SEVERE, null, ex);
        }
        String url = pro.getProperty("database.url");
        String username = pro.getProperty("database.username");
        String password = pro.getProperty("database.password");
        String databaseName = pro.getProperty("database.name");
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            return DriverManager.getConnection(url+"databaseName="+databaseName
                    +"",username,password);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
