/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.HocSinh;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hoang
 */
public class HocSinhDAO implements DAO<HocSinh, String>{
    private static Connection conn = DBConection.getConnection();
    private static HocSinhDAO instance = new HocSinhDAO();
    public static HocSinhDAO instance(){return instance;};

    @Override
    public List<HocSinh> findAll() {
        List<HocSinh> list = new ArrayList<>();//Tạo sanh sách học sinh rỗng
        String sql = "SELECT * FROM HOCSINH";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            ResultSet rs = pr.executeQuery();//Thực hiện truy vấn
            while(rs.next()){//Duyệt các bản ghi kết quả của câu SQL
                HocSinh hs = new HocSinh();//Tạo 1 học sinh mới
                //Lấy dữ liệu từ kết quả sql gán vào học sinh
                hs.setMahs(rs.getString("mahs"));
                hs.setTenhs(rs.getString("tenhs"));
                hs.setMalop(rs.getInt("malop"));
                hs.setGioitinh(rs.getString("gioitinh"));
                hs.setNgaysinh(rs.getDate("ngaysinh"));
                hs.setQuequan(rs.getString("quequan"));
                
                //Thêm học sinh vào list
                list.add(hs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;//trả về danh sách học sinh
    }

    public ArrayList<HocSinh> findByMaLop(int id) {
        ArrayList<HocSinh> list = new ArrayList<>();
        String sql = "SELECT * FROM HOCSINH where malop=?";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setInt(1, id);
            ResultSet rs = pr.executeQuery();
            while(rs.next()){//Nếu có kết quả trả về
                HocSinh hs = new HocSinh();//Tạo 1 học sinh mới
                //Lấy dữ liệu từ kết quả sql gán vào học sinh
                hs.setMahs(rs.getString("mahs"));
                hs.setTenhs(rs.getString("tenhs"));
                hs.setMalop(rs.getInt("malop"));
                hs.setGioitinh(rs.getString("gioitinh"));
                hs.setNgaysinh(rs.getDate("ngaysinh"));
                hs.setQuequan(rs.getString("quequan"));
                
                list.add(hs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    @Override
    public HocSinh findByID(String id) {
        String sql = "SELECT * FROM HOCSINH where mahs=?";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setString(1, id);//Lấy id gán váo dấu ? thứ 1;
            ResultSet rs = pr.executeQuery();
            if(rs.next()){//Nếu có kết quả trả về
                HocSinh hs = new HocSinh();//Tạo 1 học sinh mới
                //Lấy dữ liệu từ kết quả sql gán vào học sinh
                hs.setMahs(rs.getString("mahs"));
                hs.setTenhs(rs.getString("tenhs"));
                hs.setMalop(rs.getInt("malop"));
                hs.setGioitinh(rs.getString("gioitinh"));
                hs.setNgaysinh(rs.getDate("ngaysinh"));
                hs.setQuequan(rs.getString("quequan"));
                
                return hs;//Trả về học sinh
            }
        } catch (SQLException ex) {
            Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;//Không tìm thấy học sinh trả về null
    }

    @Override
    public boolean insert(HocSinh model) {
        String sql = "INSERT INTO HOCSINH(tenhs,malop,gioitinh,ngaysinh,quequan) "
                + " VALUES(?,?,?,?,?)";
        //Lưu ý, tất cả các loại mã bên SQL tự sinh nên không cần gán mã;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            //Lấy từng thuộc tính của học sinh gán cho các dấu ? trong câu sql,
            //Chú ý kiểu dữ liệu
            pr.setString(1, model.getTenhs());
            pr.setInt(2, model.getMalop());
            pr.setString(3, model.getGioitinh());
            pr.setDate(4, model.getNgaysinh());
            pr.setString(5, model.getQuequan());
            
            //Thực thi câu lệnh và lấy số lượng bản ghi vằ thêm vào
            int sl = pr.executeUpdate();
            if(sl>0) return true; //Nếu số lượng bản ghi lớn hơn 0(Insert thành công) thì trả về true
            
        } catch (SQLException ex) {
            Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;//Trả về false nếu có bất kì lỗi nào xảy ra
        }
        return false;//Trả về false nếu Update thất bại
    }

    @Override
    public boolean delete(String modelID) {
        String sql = "DELETE FROM HOCSINH WHERE mahs=?";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setString(1, modelID);//Lấy id gán váo dấu ? thứ 1;
            int sl = pr.executeUpdate();//Tương tự trên
            if(sl>0) return true;
        } catch (SQLException ex){
            ex.printStackTrace();//tương tự Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    @Override
    public boolean update(HocSinh model) {
        String sql = "UPDATE HOCSINH "
                + "SET tenhs=?,malop=?,gioitinh=?, ngaysinh=?, quequan=? where mahs=?";//Chú ý thứ tự
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setString(1, model.getTenhs());
            pr.setInt(2, model.getMalop());
            pr.setString(3, model.getGioitinh());
            pr.setDate(4, model.getNgaysinh());
            pr.setString(5, model.getQuequan());
            pr.setString(6, model.getMahs());
            int sl = pr.executeUpdate();//Tương tự trên
            if(sl>0) return true;
        } catch (SQLException ex) {
            //Logger.getLogger(HocSinhDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }
    
}
