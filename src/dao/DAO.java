/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;

/**
 *
 * @author Hoang
 * @param <Model> lớp entity
 * @param <ID> Kiểu dữ liệu cho ID
 */

public interface DAO <Model, ID> {
    List<Model> findAll();
    Model findByID(ID id);
    boolean insert(Model model);
    boolean delete(ID modelID);
    boolean update(Model model);
}
