/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.GiangDay;
import entities.Lop;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hoang
 */
public class GiangDayDAO{
    private static final Connection conn = DBConection.getConnection();
    public static ArrayList<GiangDay> findByGV(String magv){
        ArrayList<GiangDay> list = new ArrayList<>();
        String sql = "select * from GIANGDAY where magv=?";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setString(1, magv);
            ResultSet rs = pr.executeQuery();
            while(rs.next()){
                GiangDay gd = new GiangDay();
                gd.setMaGiaoVien(rs.getString("magv"));
                gd.setMaLop(rs.getInt("malop"));
                gd.setMaMonHoc(rs.getInt("mamon"));
                gd.setNamhoc(rs.getInt("hocky"));
                list.add(gd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GiangDayDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public static GiangDay findID(int malop, int mamon){
        ArrayList<GiangDay> list = new ArrayList<>();
        String sql = "select * from giangday where malop=? and mamon=?";
        PreparedStatement pr;
        try {
            pr = conn.prepareStatement(sql);
            pr.setInt(1, malop);
            pr.setInt(2, mamon);
            ResultSet rs = pr.executeQuery();
            if(rs.next()){
                GiangDay gd = new GiangDay();
                gd.setMaGiaoVien(rs.getString("magv"));
                gd.setMaLop(rs.getInt("malop"));
                gd.setMaMonHoc(rs.getInt("mamon"));
                gd.setNamhoc(rs.getInt("hocky"));
                return gd;
            }
        } catch (SQLException ex) {
            Logger.getLogger(GiangDayDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static boolean insert(GiangDay gd){
        String sql = "insert into giangday(malop,mamon,magv,hocky)"
                + " values(?,?,?,?)";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setInt(1, gd.getMaLop());
            pr.setInt(2, gd.getMaMonHoc());
            pr.setString(3, gd.getMaGiaoVien());
            pr.setInt(4,gd.getNamhoc());
            return pr.executeUpdate()>0;
        } catch (SQLException ex) {
            Logger.getLogger(GiangDayDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public static boolean update(GiangDay gd){
        String sql = "update giangday set malop=?, mamon=?, magv=?,hocky=?";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setInt(1, gd.getMaLop());
            pr.setInt(2, gd.getMaMonHoc());
            pr.setInt(4, gd.getNamhoc());
            pr.setString(3, gd.getMaGiaoVien());
            return pr.executeUpdate()>0;
        } catch (SQLException ex) {
            Logger.getLogger(GiangDayDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public static boolean delete(GiangDay gd){
        String sql = "delete from giangday where malop=? and mamon=? and magv=?";
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            pr.setInt(1, gd.getMaLop());
            pr.setInt(2, gd.getMaMonHoc());
            pr.setString(3, gd.getMaGiaoVien());
            return pr.executeUpdate()>0;
        } catch (SQLException ex) {
            Logger.getLogger(GiangDayDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
