/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;


/**
 *
 * @author Hoang
 */

public class TaiKhoan {

    
    private String taikhoan;
    
    private String matkhau;
    private String vaitro;
    private Boolean logon;

    public String getTaikhoan() {
        return taikhoan;
    }

    public void setTaikhoan(String taikhoan) {
        this.taikhoan = taikhoan;
    }

    public String getMatkhau() {
        return matkhau;
    }

    public void setMatkhau(String matkhau) {
        this.matkhau = matkhau;
    }

    public String getVaitro() {
        return vaitro;
    }

    public void setVaitro(String vaitro) {
        this.vaitro = vaitro;
    }

    public Boolean getLogon() {
        return logon;
    }

    public void setLogon(Boolean logon) {
        this.logon = logon;
    }
    
}
