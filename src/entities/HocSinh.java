/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Date;

 /*
 * @author Hoang
 */

public class HocSinh {
 
    private static final long serialVersionUID = 1L;
   
    
    private String mahs;
    private String tenhs;
    private String quequan;
    private Date ngaysinh;
    private String gioitinh;
    private int malop;


    public String getMahs() {
        return mahs;
    }

    public void setMahs(String mahs) {
        this.mahs = mahs;
    }

    public String getTenhs() {
        return tenhs;
    }

    public void setTenhs(String tenhs) {
        this.tenhs = tenhs;
    }

    public String getQuequan() {
        return quequan;
    }

    public void setQuequan(String quequan) {
        this.quequan = quequan;
    }

    public Date getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(Date ngaysinh) {
        this.ngaysinh = ngaysinh;
    }

    public String getGioitinh() {
        return gioitinh;
    }

    public void setGioitinh(String gioitinh) {
        this.gioitinh = gioitinh;
    }

    public int getMalop() {
        return malop;
    }

    public void setMalop(int malop) {
        this.malop = malop;
    }

    @Override
    public String toString() {
        return "HocSinh{" + "mahs=" + mahs + ", tenhs=" + tenhs + ", quequan=" + quequan + ", ngaysinh=" + ngaysinh + ", gioitinh=" + gioitinh + ", malop=" + malop + '}';
    }

    
   
}
