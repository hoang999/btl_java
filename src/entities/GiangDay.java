package entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class GiangDay  {


    private int namhoc;

    private String maGiaoVien;

    private int maLop;

    private int maMonHoc;

    public int getNamhoc() {
        return namhoc;
    }

    public void setNamhoc(int namhoc) {
        this.namhoc = namhoc;
    }

    public String getMaGiaoVien() {
        return maGiaoVien;
    }

    public void setMaGiaoVien(String maGiaoVien) {
        this.maGiaoVien = maGiaoVien;
    }

    public int getMaLop() {
        return maLop;
    }

    public void setMaLop(int maLop) {
        this.maLop = maLop;
    }

    public int getMaMonHoc() {
        return maMonHoc;
    }

    public void setMaMonHoc(int maMonHoc) {
        this.maMonHoc = maMonHoc;
    }

    @Override
    public String toString() {
        return "GiangDay{" + "namhoc=" + namhoc + ", maGiaoVien=" + maGiaoVien + ", maLop=" + maLop + ", maMonHoc=" + maMonHoc + '}';
    }
    
}
